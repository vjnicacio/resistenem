<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="pt-BR">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- ÍCONE DA PÁGINA -->
<link rel="shortcut icon" href="./imgs/re.png" type="image/x-icon" />

<!-- BIBLIOTECA CSS -->
<link rel="stylesheet"
	href="https://landkit.goodthemes.co/assets/css/libs.bundle.css" />

<!-- TEMA CSS -->
<link rel="stylesheet"
	href="https://landkit.goodthemes.co/assets/css/theme.bundle.css" />

<link href="http://fonts.cdnfonts.com/css/lemonmilk" rel="stylesheet">

<!-- TÍTULO DA PÁGINA -->
<title>Boas-vindas ao Resistenem</title>

<style>
.nav-link {
	color: #E94547;
	display: block;
	padding: 0.5rem 1rem;
	transition: color .15s ease-in-out, background-color .15s ease-in-out,
		border-color .15s ease-in-out;
}

a {
	color: #E94547;
	text-decoration: none;
}

a:hover {
	color: #E94547;
}

#btn-back-to-top {
	position: fixed;
	bottom: 20px;
	right: 20px;
	display: none;
}

.cards-wrapper {
	display: flex;
	justify-content: center;
}

.card img {
	max-width: 100%;
	max-height: 100%;
}

.card {
	margin: 0 0.5em;
	box-shadow: 2px 6px 8px 0 rgba(22, 22, 26, 0.18);
	border: none;
	border-radius: 0;
}

.carousel-inner {
	padding: 1em;
}

.carousel-control-prev, .carousel-control-next {
	background-color: #e1e1e1;
	width: 5vh;
	height: 5vh;
	border-radius: 50%;
	top: 50%;
	transform: translateY(-50%);
}

@media ( min-width : 768px) {
	.card img {
		height: 11em;
	}
}
</style>

</head>
<body class="bg-light">

	<div class="modal modal-fullscreen fade" id="modal-fullscreen"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<button type="button" class="close" data-dismiss="modal">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-body">
					<iframe src="https://www.youtube.com/embed/o3zbrHCsM3c"
						frameborder="0" webkitallowfullscreen mozallowfullscreen
						allowfullscreen></iframe>

				</div>
				<div class="modal-footer">
					<p>Fanfare at San Pedro Gateway, Port of Los Angeles,
						California, USA</p>
					<p>July 2008</p>
					<!--         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				</div>
			</div>
		</div>
	</div>