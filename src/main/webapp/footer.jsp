<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>



<footer class="py-8 py-md-11 bg-white">
	<div class="row">

		<div class="col-12 col-md-1"></div>

		<div class="col-12 col-md-3">

			<img src="./imgs/resistenem.png" alt="..."
				class="footer-brand img-fluid mb-2">

			<p class="text-gray-700 mb-2">A Educação é a Resistência.</p>

			<ul class="list-unstyled list-inline list-social mb-6 mb-md-0">
				<li class="list-inline-item list-social-item me-3"><a href="#!"
					class="text-decoration-none"> <img
						src="https://landkit.goodthemes.co/assets/img/icons/social/instagram.svg"
						class="list-social-icon" alt="...">
				</a></li>
				<li class="list-inline-item list-social-item me-3"><a href="#!"
					class="text-decoration-none"> <img
						src="https://landkit.goodthemes.co/assets/img/icons/social/facebook.svg"
						class="list-social-icon" alt="...">
				</a></li>
				<li class="list-inline-item list-social-item me-3"><a href="#!"
					class="text-decoration-none"> <img
						src="https://landkit.goodthemes.co/assets/img/icons/social/twitter.svg"
						class="list-social-icon" alt="...">
				</a></li>
				<li class="list-inline-item list-social-item"><a href="#!"
					class="text-decoration-none"> <img
						src="https://landkit.goodthemes.co/assets/img/icons/social/pinterest.svg"
						class="list-social-icon" alt="...">
				</a></li>
			</ul>

		</div>
		<div class="col-12 col-md-2">

			<ul class="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
				<li class="mb-3"><a data-scroll="{&quot;offset&quot;: 0}"
					href="#sectionOne" class="text-reset">HOME </a></li>
				<li class="mb-3"><a href="#!" class="text-reset">
						INSTITUCIONAL </a></li>
				<li class="mb-3"><a href="#!" class="text-reset">
						VOLUNTARIOS </a></li>
			</ul>

		</div>

		<div class="col-12 col-md-2">

			<ul class="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
				<li class="mb-3"><a href="#!" class="text-reset">MATERIAIS
				</a></li>
				<li class="mb-3"><a href="#!" class="text-reset"> FACA
						PARTE </a></li>
				<li class="mb-3"><a href="#!" class="text-reset"> CONTATO </a></li>
			</ul>

		</div>

		<div class="col-12 col-md-3">

			<ul class="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
				<li class="mb-3"><a href="#!" class="text-reset small"><i
						class="fe fe-arrow-mail text-muted"></i> resistenem@email.com </a></li>
				<li class="mb-3"><a href="#!" class="text-reset small">
						@resistenem </a></li>
			</ul>

		</div>

		<div class="col-12 col-md-1"></div>

	</div>
</footer>


<!-- Map JS -->
<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>

<!-- LIB JS -->
<script src="https://landkit.goodthemes.co/assets/js/vendor.bundle.js"></script>

<!-- TEMA JS -->
<script src="https://landkit.goodthemes.co/assets/js/theme.bundle.js"></script>

<!-- VIDEO MODAL JS -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

<!-- VIDEO MODAL JS -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script>
	//.modal-backdrop classes

	$(".modal-transparent").on('show.bs.modal', function() {
		setTimeout(function() {
			$(".modal-backdrop").addClass("modal-backdrop-transparent");
		}, 0);
	});
	$(".modal-transparent").on('hidden.bs.modal', function() {
		$(".modal-backdrop").addClass("modal-backdrop-transparent");
	});

	$(".modal-fullscreen").on('show.bs.modal', function() {
		setTimeout(function() {
			$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
		}, 0);
	});
	$(".modal-fullscreen").on('hidden.bs.modal', function() {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});
</script>

<script>
	//Get the button
	let mybutton = document.getElementById("btn-back-to-top");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {
		scrollFunction();
	};

	function scrollFunction() {
		if (document.body.scrollTop > 20
				|| document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}
	// When the user clicks on the button, scroll to the top of the document
	mybutton.addEventListener("click", backToTop);
</script>

</body>
</html>