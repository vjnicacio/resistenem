<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.*"%>

<nav id="sectionOne"
	class="navbar navbar-expand-lg navbar-light bg-white">
	<div class="container">

		<!-- Brand -->
		<a class="navbar-brand" href="./index.jsp"> <img
			src="./imgs/resistenem.png" class="navbar-brand-img" alt="...">
		</a>

		<!-- Toggler -->
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
			data-bs-target="#navbarCollapse" aria-controls="navbarCollapse"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!-- Collapse -->
		<div class="collapse navbar-collapse" id="navbarCollapse">

			<!-- Toggler -->
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
				aria-controls="navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<i class="fe fe-x"></i>
			</button>

			<ul class="navbar-nav ms-auto">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="index.jsp" aria-haspopup="true"
					aria-expanded="false"> HOME </a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="#" aria-haspopup="true"
					aria-expanded="false"> INSTITUCIONAL </a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="#" aria-haspopup="true"
					aria-expanded="false"> VOLUNT�RIOS </a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="#" aria-haspopup="true"
					aria-expanded="false"> MATERIAIS </a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="#" aria-haspopup="true"
					aria-expanded="false"> FA�A PARTE </a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="navbarLandings"
					data-bs-toggle="dropdown" href="#" aria-haspopup="true"
					aria-expanded="false"> CONTATO </a></li>
			</ul>

		</div>

	</div>
</nav>

<!-- MENU -->