<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@ taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.*"%>

<jsp:include page="header.jsp" />

<jsp:include page="menu.jsp" />

<a id="btn-back-to-top"
	class="btn btn-white btn-floating btn-lg btn-rounded-circle shadow"
	data-scroll="{&quot;offset&quot;: 0}" href="#sectionOne"> <i
	class="fe fe-arrow-up text-danger"></i>
</a>

<!-- SE��O PRINCIPAL -->
<section class="mt-n10 align-items-center"
	style="background-position: 58.33325% center; background-color: white; background-image: url('./imgs/Home.svg'); background-repeat: no-repeat; background-attachment: scroll; background-size: contain;">
	<div class="container d-flex flex-column">
		<div
			class="row align-items-center justify-content-center min-vh-100 py-8 py-md-11">
			<div class="col-12 col-md-8 col-lg-7 mt-auto text-center">

				<h1 class="display-1 fw-bold text-white mb-6 mt-n3">

					<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />

				</h1>

			</div>
			<div class="col-12 col-md-12 text-center">

				<div class="col-12 mt-auto text-center">

					<!-- Button -->
					<a class="btn btn-danger btn-rounded-circle shadow"
						data-scroll="{&quot;offset&quot;: 0}" href="#sectionTwo"> <i
						class="fe fe-arrow-down"></i>
					</a> <br /> <br /> <br /> <br />

				</div>
			</div>
		</div>
	</div>
</section>
<!-- SE��O PRINCIPAL -->

<!-- SE��O PRINCIPAL -->
<section class="py-8 pt-md-11 pb-md-12"
	style="background-color: #E94547;">
	<div class="container d-flex flex-column">
		<div
			class="row align-items-center justify-content-center min-vh-100 py-8 py-md-11">
			<div class="col-12 col-md-12 mt-auto text-justify">

				<!-- Heading -->
				<h2 id="sectionTwo" class="display-3 mb-0 fw-bold">
					<br /> <br /> <span class="text-white"
						style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">&nbsp;&nbsp;QUEM

						<hr
							style="position: absolute; width: 803px; height: 0px; left: 0px; top: 145.93px; border: 2px solid #FFFFFF; margin-left: -628px;" />

						<strong><br />SOMOS</strong>
					</span>
				</h2>

				<p class="fs-lg text-muted mb-6"></p>

				<p class="fs-lg text-white mb-6">Em meio ao cen�rio pand�mico de
					2020, um grupo de volunt�rios se mobilizou para construir o
					ResistEnem, projeto social que tem por objetivo estimular a
					populariza��o da educa��o por meio do uso de aplicativo multim�dia
					de mensagens instant�neas.</p>
				<p class="fs-lg text-white mb-6">Atualmente, a internet � a
					fonte principal de comunica��o da ONG, mais de XX alunos j� foram
					aprovados em universidades p�blicas e privadas em todo pa�s.</p>
				<p class="fs-lg text-white mb-6">Mais de 800 alunos passaram
					pelo projeto (2020-2021), contamos com 140 volunt�rios, espalhados
					em diferentes regi�es do Brasil e at� de outros pa�ses, que auxilia
					vestibulandos de baixa renda na prepara��o para o Exame Nacional do
					Ensino M�dio (ENEM).</p>
				<p class="fs-lg text-white mb-6">O ResistENEM tem trabalhado
					alinhado aos Objetivos de Desenvolvimento Sustent�vel (ODS) da ONU,
					que abordam os principais desafios de desenvolvimento enfrentados
					por pessoas no Brasil e no mundo.</p>

			</div>
			<div class="col-12 mt-auto text-center">

				<!-- Button -->
				<a class="btn btn-white btn-rounded-circle shadow"
					data-scroll="{&quot;offset&quot;: 0}" href="#sectionThree"> <i
					class="fe fe-arrow-down text-danger"></i>
				</a>

			</div>
		</div>
		<!-- / .row -->
	</div>
	<!-- / .container -->
</section>
<!-- SE��O PRINCIPAL -->

<section class="py-8 pt-md-11 pb-md-12" id="sectionThree">
	<div class="container">

		<div class="row">
			<div class="col-12 col-md-12 text-center">
				<h2 class="display-1 mb-0 fw-bold">
					<span class="text-black"
						style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">NOSSOS<br />
						<strong>N�MEROS</strong></span>
				</h2>
			</div>
		</div>

		<div class="row">
			<br> <br> <br> <br>
		</div>

		<div class="row">
			<div class="col-12 col-md-4 text-center">

				<!-- Heading -->
				<h1 class="display-2 fw-bold text-danger">
					<span data-countup="{&quot;startVal&quot;: 0}" data-to="800"
						style="font-size: 96px" data-aos="" data-aos-id="countup:in"
						class="aos-init aos-animate">-</span>
				</h1>

				<!-- Text -->
				<p class="text-black mb-6 mb-md-0">Alunos</p>

			</div>
			<div class="col-12 col-md-4 text-center">

				<!-- Heading -->
				<h1 class="display-2 fw-bold text-danger">
					<span data-countup="{&quot;startVal&quot;: 0}" data-to="140"
						style="font-size: 96px" data-aos="" data-aos-id="countup:in"
						class="aos-init aos-animate">-</span>
				</h1>

				<!-- Text -->
				<p class="text-black mb-6 mb-md-0">Voluntarios</p>

			</div>
			<div class="col-12 col-md-4 text-center">

				<!-- Heading -->
				<h1 class="display-2 fw-bold text-danger">
					<span data-countup="{&quot;startVal&quot;: 0}" data-to="2"
						style="font-size: 96px" data-aos="" data-aos-id="countup:in"
						class="aos-init aos-animate">-</span>
				</h1>

				<!-- Text -->
				<p class="text-black mb-0">Premios *</p>

			</div>
		</div>
		<!-- / .row -->

		<div class="row">
			<div class="col-12 col-md-12 text-center">

				<br> <br> <br> <br>

				<p class="fs-lg text-black mb-6 small">*Lorem ipsum dolor sit
					amet consectur adpiscing esit.</p>

				<br> <br> <br> <br>

			</div>
		</div>

		<div class="col-12 mt-auto text-center">

			<!-- Button -->
			<a class="btn btn-danger btn-rounded-circle shadow"
				data-scroll="{&quot;offset&quot;: 0}" href="#sectionFour"> <i
				class="fe fe-arrow-down"></i>
			</a>

		</div>

	</div>
	<!-- / .container -->
</section>

<section id="sectionFour" class="pt-10 pt-lg-9 pb-md-14 pb-lg-15"
	style="background-color: #000;">
	<div class="container">

		<div class="row">
			<div class="col-12 col-md-12 text-center">
				<h2 class="display-1 mb-0 fw-bold" class="text-white"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					<span class="text-white">ALUNOS</span><br /> <span
						class="text-white strong"><strong>APROVADOS</strong></span>

				</h2>
				<p class="text-white mb-6">
					<br /> Em um ano de projeto foram <strong>mais de 40
						aprovados</strong> em Universidades Federais e Estaduais por todo pa�s.
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-lg-3 text-center">
				<div class="icon icon-lg mb-4">
					<img src="./imgs/ic_person_man.png" alt="..."
						style="h width: 100%; max-width: 244px;"
						class="avatar-img rounded-circle">
				</div>
				<h3 class="fw-bold text-white">
					<br />Mariana Rocha
				</h3>
				<p class="text-white mb-8 small">Letras (USP)</p>
			</div>

			<div class="col-12 col-lg-3 text-center">
				<div class="icon icon-lg mb-4">
					<img src="./imgs/ic_person_woman.png" alt="..."
						style="h width: 100%; max-width: 244px;"
						class="avatar-img rounded-circle">
				</div>
				<h3 class="fw-bold text-white">
					<br />Mariana Rocha
				</h3>
				<p class="text-white mb-8 small">Letras (USP)</p>
			</div>

			<div class="col-12 col-lg-3 text-center">
				<div class="icon icon-lg mb-4">
					<img src="./imgs/ic_person_man.png" alt="..."
						style="h width: 100%; max-width: 244px;"
						class="avatar-img rounded-circle">
				</div>
				<h3 class="fw-bold text-white">
					<br />Mariana Rocha
				</h3>
				<p class="text-white mb-8 small">Letras (USP)</p>
			</div>

			<div class="col-12 col-lg-3 text-center">
				<div class="icon icon-lg mb-4">
					<img src="./imgs/ic_person_woman.png" alt="..."
						style="h width: 100%; max-width: 244px;"
						class="avatar-img rounded-circle">
				</div>
				<h3 class="fw-bold text-white">
					<br />Mariana Rocha
				</h3>
				<p class="text-white mb-8 small">Letras (USP)</p>
			</div>

		</div>

		<div class="row">
			<div class="col-12 col-md-12 text-center">
				<div class="col-12 mt-auto text-center">
					<a class="btn btn-danger btn-rounded-circle shadow"
						data-scroll="{&quot;offset&quot;: 0}" href="#sectionFive"> <i
						class="fe fe-arrow-down"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="sectionFive" data-jarallax="" data-speed=".8"
	class="pt-10 pt-md-14 pb-12 pb-md-15 overlay overlay-danger overlay-80 jarallax"
	style="background-color: #E94547;">

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 text-left">

				<!-- Heading -->
				<h1 class="display-1 fw-bold text-white mb-6 mt-n3"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					METODOLOGIA</h1>
				<hr
					style="position: absolute; width: 1103px; height: 0px; left: 0px; top: 50px; border: 2px solid #FFFFFF; margin-left: -628px;" />

				<p class="fs-lg text-white text-justify">Diariamente, os
					professores volunt�rios compartilham, nos grupos de aula, conte�dos
					exclusivos sobre as �reas de estudo da maior porta de entrada ao
					ensino superior, o Enem.</p>

				<p class="fs-lg text-white text-justify">Os materiais s�o
					enviados de segunda a s�bado em um cronograma de dois blocos; o
					primeiro conta com material de apoio para estudo, exerc�cios e
					gabarito comentado. No segundo bloco, que ocorre no fim de cada dia
					e aos finais de semana, os alunos tiram d�vidas e plant�es de
					d�vida para os alunos.</p>

				<p class="fs-lg text-white text-justify">O uso de um aplicativo
					de mensagens instant�neas como plataforma principal do projeto foi
					pensado � medida que as tecnologias digitais hoje s�o mais
					acess�veis, uma vez que podem ser utilizadas para aprender em
					qualquer lugar ou tempo, e de m�ltiplas formas.</p>


			</div>

			<div class="col-12 col-md-6 text-left">

				<iframe width="644" height="362"
					src="https://www.youtube.com/embed/o3zbrHCsM3c&feature=youtu.be">
				</iframe>

				<a class="btn btn-pill btn-white text-danger shadow lift"
					data-bs-toggle="modal" data-bs-target="#modal-fullscreen" href="#">
					<i class="fe fe-eye me-2"></i> Assista nosso v�deo
				</a>

			</div>

		</div>

		<div class="row">
			<div class="col-12 col-md-12 text-center">

				<br> <br> <br> <br>

				<div class="col-12 mt-auto text-center">

					<!-- Button -->
					<a class="btn btn-white text-danger btn-rounded-circle shadow"
						data-scroll="{&quot;offset&quot;: 0}" href="#sectionSix"> <i
						class="fe fe-arrow-down"></i>
					</a>

				</div>

				<br> <br> <br> <br>

			</div>
		</div>

		<!-- / .row -->
	</div>
	<!-- / .container -->
</section>


<section id="sectionSix" class="pt-12 pb-md-14 pb-lg-15 bg-white">
	<div class="container">

		<div class="row">
			<div class="col-12 col-md-12 text-center">
				<h2 class="display-1 mb-0 fw-bold"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					�REAS DO <br />ENEM
				</h2>

			</div>
		</div>
	</div>

	<div class="row align-items-center justify-content-between">
		<br>
	</div>

	<div class="row align-items-center justify-content-between">

		<div class="col-12 col-md-1 text-center"></div>

		<div class="col-12 col-md-10 text-center">
			<div id="carouselExampleControls" class="carousel slide"
				data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="cards-wrapper">
							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">LINGUAGENS, CODIGOS E
												SUAS TECNOLOGIAS</h3>
											<p class="text-white mb-8 mb-lg-0">Lingua Portuguesa,
												Literatura, Lingua Estrangeira (Ingles ou Espanhol), Artes,
												Educacao Fisica e Tecnologias da Informacao e Comunicacao.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">CIENCIAS HUMANAS E SUAS
												TECNOLOGIAS</h3>
											<p class="text-white mb-8 mb-lg-0">Historia, Geografia,
												Filosofia e Sociologia.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">CIENCIAS DA NATUREZA E
												SUAS TECNOLOGIAS</h3>
											<p class="text-white mb-8 mb-lg-0">Biologia, Quimica e
												Fisica.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">MATEMATICA SUAS
												TECNOLOGIAS</h3>
											<p class="text-white mb-8 mb-lg-0">Matematica.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-12 col-md-1 text-center"></div>

	</div>

	<div class="row">
		<div class="col-12 col-md-12 text-center">

			<br> <br> <br> <br>

			<div class="col-12 mt-auto text-center">

				<!-- Button -->
				<a class="btn btn-danger btn-rounded-circle shadow"
					data-scroll="{&quot;offset&quot;: 0}" href="#sectionSeven"> <i
					class="fe fe-arrow-down"></i>
				</a>

			</div>

			<br> <br> <br> <br>

		</div>
	</div>
	<!-- / .container -->
</section>

<section id="sectionSeven"
	style="background-image: url(./imgs/fundo.png); no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; background-size: cover; -o-background-size: cover;">
	<div class="container d-flex flex-column">
		<div
			class="row align-items-center justify-content-center min-vh-100 py-8 py-md-11">
			<div class="col-12 col-md-12 text-center">

				<h1 class="display-3 text-black"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					SEJA UM<br /> <strong>ALUNO</strong>
				</h1>

				<p class="mb-5 fs-lg text-black text-left">O ResistEnem recebe
					inscri��es de alunos de diferentes perfis e de todo o pa�s. Se voc�
					est� concluindo o ensino m�dio e tem mais tempo para estudar ou se
					j� concluiu e tem pouco tempo para estudar, vem com a gente que
					nossos professores volunt�rios ter�o prazer em ajudar nessa jornada
					rumo a realiza��o de um sonho: cursar uma universidade.</p>

				<a class="btn btn-danger text-white rounded-pill"
					href="https://docs.google.com/forms/d/e/1FAIpQLSfwnugSSPr_mQZySCcJRGxkCp-eL6ARVPudk4fJ7oUZAgbRnQ/viewform"
					target="_blank"> Seja um aluno! <i
					class="fe fe-arrow-right ms-2"></i>
				</a>

			</div>
		</div>
		<div class="col-12 col-md-12 text-center">

			<div class="col-12 mt-auto text-center">

				<!-- Button -->
				<a class="btn btn-danger btn-rounded-circle shadow"
					data-scroll="{&quot;offset&quot;: 0}" href="#sectionEight"> <i
					class="fe fe-arrow-down"></i>
				</a> <br /> <br /> <br /> <br />

			</div>
		</div>
	</div>
</section>

<section class="py-8 py-md-11 bg-light" id="sectionEight">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">

				<h1 class="display-3 text-black text-center"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					AREAS<br /> <strong>ORGANIZACIONAIS</strong>
				</h1>

				<p class=" text-black">O ResistEnem tem em sua ess�ncia a
					multidisciplinaridade. Ele � feito por volunt�rios de todo o pa�s e
					de diversas �reas do conhecimento que atendem todo o curr�culo do
					Exame Nacional do Ensino M�dio, al�m das �reas administrativas da
					ONG. Professores, profissionais</p>

			</div>
		</div>
	</div>
	<!-- / .row -->
	<div class="row justify-content-center">
		<div class="col-12 col-md-10 col-lg-8">


			<div id="carouselExampleControls" class="carousel slide"
				data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="cards-wrapper">
							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">VAGA DE GEST�O DE PESSOAS</h3>
											<p class="text-white mb-8 mb-lg-0">Transmitir a ess�ncia
												da ONG para os alunos e volunt�rios. Trabalhando o
												endomarketing com engajamento e motiva��o.</p>
										</div>
									</div>
								</div>
							</div>

							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">VAGA DE COMUNICA��O E
												TECNOLOGIAS</h3>
											<p class="text-white mb-8 mb-lg-0">Apoio nas �reas de
												Redes sociais, comunica��o empresarial, desenvolvimento dos
												materiais para os alunos, marketing, cria��o de ferramentas
												e solu��es para melhoria dos processos, designers e
												desenvolvedores.</p>
										</div>
									</div>
								</div>
							</div>

							<div class="card rounded-lg shadow-lg aos-init aos-animate"
								data-aos="fade-up" data-aos-delay="200"
								style="background-image: url(./imgs/bgEnemBlur.jpeg);">
								<div class="card-body py-6 py-md-8">
									<div class="row justify-content-center">
										<div class="col-12 col-xl-10">
											<h3 class="fw-bold text-white">VAGAS DE SUPORTE JURIDICO
												E PSICOPEDAGOGICO</h3>
											<p class="text-white mb-8 mb-lg-0">Entendemos que �
												importante o apoio para estudantes e volunt�rios para
												realizar um amparo, assim como orienta��es pedag�gicas para
												a constru��o de material did�tico e informativo de
												qualidade.</p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-12 col-md-12 text-center">
			<br /> <a class="btn btn-danger text-white rounded-pill"
				href="https://docs.google.com/forms/d/e/1FAIpQLSfwnugSSPr_mQZySCcJRGxkCp-eL6ARVPudk4fJ7oUZAgbRnQ/viewform"
				target="_blank"> SE INSCREVA COMO VOLUNT�RIO <i
				class="fe fe-arrow-right ms-2"></i>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-md-12 text-center">

			<br> <br> <br> <br>

			<div class="col-12 mt-auto text-center">

				<!-- Button -->
				<a class="btn btn-danger btn-rounded-circle shadow"
					data-scroll="{&quot;offset&quot;: 0}" href="#sectionNine"> <i
					class="fe fe-arrow-down"></i>
				</a>

			</div>

			<br> <br> <br> <br>

		</div>
	</div>
	<!-- / .container -->
</section>

<section id="sectionNine">
	<div class="container d-flex flex-column">
		<div
			class="row align-items-center justify-content-center min-vh-100 py-8 py-md-11">
			<div class="col-12 col-md-12 text-center">

				<h1 class="display-3 text-black"
					style="font-family: 'Lemon/Milk', sans-serif; font-family: 'Lemon/Milk light', sans-serif;">
					RESISTENEM<br /> <strong>NA MIDIA</strong>
				</h1>

				<img src="./imgs/midia.jpeg" />

			</div>
		</div>
	</div>
</section>
<jsp:include page="footer.jsp" />